package id

import (
	"encoding/json"
	"strings"
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestNewIDIsUnique(t *testing.T) {
	// Arrange and Act.
	id1 := New()
	id2 := New()

	// Assert.
	test.That(t, id1, is.NotEqualTo(Empty))
	test.That(t, id2, is.NotEqualTo(Empty))
	test.That(t, id1, is.NotEqualTo(id2))
}

func TestEmptyIDIsAllZeros(t *testing.T) {
	// Arrange and Act and Assert.
	for i := 0; i < ByteSize; i++ {
		test.That(t, Empty[i], is.EqualTo(byte(0)))
	}
}

func TestIsValid(t *testing.T) {
	// Arrange.
	id := New()

	// Act and Assert.
	test.That(t, id.IsValid(), is.True())
	test.That(t, Empty.IsValid(), is.False())
}

func TestStringAndParseSymmetric(t *testing.T) {
	// Arrange.
	id1 := New()

	// Act.
	hexID := id1.String()
	id2, err := Parse(hexID)

	// Assert.
	test.That(t, err, is.Nil())
	test.That(t, id2, is.EqualTo(id1))
}

func TestParseInvalidHex(t *testing.T) {
	// Arrange.
	hexID := strings.Repeat("F", 31) + "S"

	// Act.
	id, err := Parse(hexID)

	// Assert.
	test.That(t, id, is.EqualTo(Empty))
	test.That(t, err, is.NotNil())
}

func TestParseWrongByteSize(t *testing.T) {
	// Arrange.
	hexID := strings.Repeat("F", 34)

	// Act.
	id, err := Parse(hexID)

	// Assert.
	test.That(t, id, is.EqualTo(Empty))
	test.That(t, err, is.NotNil())
}

func TestPartitioning(t *testing.T) {
	// Arrange.
	partitions := make(map[byte]int)

	// Act.
	for i := 0; i < 2_088_960; i++ {
		id := New()
		partitions[id.Partition()]++
	}

	t.Logf("%v", partitions)

	// Assert.
	test.That(t, partitions[0] < 9_000, is.True())
	test.That(t, partitions[0] > 7_000, is.True())
}

func TestMarshalAndUnmarshalSymmetric(t *testing.T) {
	// Arrange.
	x1 := &typeWithID{SomeID: New()}

	// Act.
	rawJSON, err := json.Marshal(x1)
	test.That(t, err, is.Nil())

	x2 := &typeWithID{}
	err = json.Unmarshal(rawJSON, x2)
	test.That(t, err, is.Nil())

	// Assert.
	test.That(t, x2.SomeID, is.EqualTo(x1.SomeID))
}

func TestUnmarshalInvalidHex(t *testing.T) {
	// Arrange.
	rawInvalidID := []byte(`"FFFFFH`)

	// Act.
	newID := &ID{}
	err := newID.UnmarshalJSON(rawInvalidID)

	// Assert.
	test.That(t, err, is.NotNil())
}

func TestValue(t *testing.T) {
	// Arrange.
	id := New()

	// Act.
	val, err := id.Value()

	// Assert.
	test.That(t, err, is.Nil())

	idStr, ok := val.(string)
	test.That(t, ok, is.True())
	test.That(t, idStr, is.EqualTo(id.String()))
}

func TestScanNil(t *testing.T) {
	// Arrange.
	id := ID{}

	// Act.
	err := id.Scan(nil)

	// Assert.
	test.That(t, err, is.Nil())
	test.That(t, id, is.EqualTo(Empty))
}

func TestScanNonString(t *testing.T) {
	// Arrange.
	id := ID{}

	// Act.
	err := id.Scan(123)

	// Assert.
	test.That(t, err, is.NotNil())
	test.That(t, err.Error(), is.EqualTo("expected to scan a string to ID, but was not a string"))
}

func TestScanNonHexString(t *testing.T) {
	// Arrange.
	id := ID{}

	// Act.
	err := id.Scan("Hello, World!")

	// Assert.
	test.That(t, err, is.NotNil())
}

func TestScanSuccess(t *testing.T) {
	// Arrange.
	id := ID{}

	// Act.
	err := id.Scan(New().String())

	// Assert.
	test.That(t, err, is.Nil())
	test.That(t, id, is.NotEqualTo(Empty))
}

// -----------------------------------------------------------------------------

type typeWithID struct {
	SomeID ID
}

![](./icon.png)

# id

Package `id` defines a unique 128-bit identifier type.

## Usage Example

```go
userID := id.New()
fmt.Printf("%v", userID) // 15f15827e9d80cf0795514a0cffc4006
```
